<footer class="footer">
  <div class="container">
    <div class="row">

  <div class="col-xl-3 col-12"><?php dynamic_sidebar('f1'); ?>
		</div>
  <div class="col-xl-6 col-12"> <?php dynamic_sidebar('f2'); ?></div>
	<div class="col-xl-3 col-12" ><?php
				wp_nav_menu(array(
						'theme_location'    => 'footer',
				));
				?></div>
  </div>
  </div>
</footer>
<div id="strap" >
<div class="container">
<div class="row" >
	<div class="col">

		<?php //add legal menu container
		wp_nav_menu(array(
		    'theme_location'  => 'legal',
		    'depth'              => 1, // 1 = no dropdowns, 2 = with dropdowns.
		    'container'       => 'div',
		    'container_class' => 'collapse navbar-collapse',
		    'container_id'    => 'legal',
		    'menu_class'      => 'navbar-nav mr-auto',

		));
		?>

	</div>
	  <div class="col social"> <?php  require_once get_template_directory() . '/templates/functions_social.php';?></div>

</div>
</div>
</div>


<div id="strap" >
<div class="container">
<div class="row" >

  <div class="col copyright"> COPYRIGHT 2019 CINQ.
  </div>
</div>
</div>
</div>

</div>
<?php wp_footer(); ?>
</body>
  </html>
