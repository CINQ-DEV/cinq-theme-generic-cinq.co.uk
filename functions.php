<?php

function _themename_assets()
{

//wp_enqueue_style( '_bootstrap-stylesheet', get_template_directory_uri() . '/dist/vendor/bootstrap/bootstrap.css', array(), '1.0.0', 'all' );//add boostrap
wp_enqueue_style('_main-stylesheet', get_template_directory_uri() . '/style.css', array(), '1.0.0', 'all');
    wp_enqueue_script('_themename-scripts-jquery', get_template_directory_uri() . '/assets/js/vendor/jquery-3.3.1.min.js', array(), '1.0.0', true);
    wp_enqueue_script('_themename-scripts', get_template_directory_uri() . '/assets/js/vendor/bootstrap.js', array(), '1.0.0', true);
    wp_enqueue_script('_themename-scripts-fontawsome', 'https://use.fontawesome.com/releases/v5.7.1/js/all.js', array(), '1.0.0', true);
    wp_enqueue_script('_themename-scripts-cinq', get_template_directory_uri() . '/assets/js/cinq.js', array(), '1.0.0', true);

// just for full screen template

// Just add the code to the main call with the check
    if (is_page_template('template-full-screen.php')) {
        wp_enqueue_style('full_screen', get_template_directory_uri() . '/assets/js/vendor/fullpage.min.css', array(), '1.0.0', 'all');
        wp_enqueue_script('full-screen', get_stylesheet_directory_uri() . '/assets/js/vendor/fullpage.min.js', array(), '1.0.0', true);
    }
}
add_action('wp_enqueue_scripts', '_themename_assets');



//ADD feature image support
add_theme_support('post-thumbnails');

// Menus

require_once get_template_directory() . '/templates/functions_menu.php';



// GET CUSTOM POST Types
require_once get_template_directory() . '/templates/functions_custom_posts.php';

// GET CUSTOM Tax Types
require_once get_template_directory() . '/templates/functions_custom_tax.php';


//Sidebars
require_once get_template_directory() . '/templates/functions_sidebars.php';
