<?php
// Register Custom Navigation Walker
require_once get_template_directory() . '/templates/functions_bootstrap_navwalker.php';
//resolve wp to boostrap menu.
register_nav_menus(array(
    'primary' => __('Primary Menu', 'THEMENAME'),
    'footer' => __('Footer Menu', 'THEMENAME'),
        'legal' => __('Legal Menu', 'THEMENAME'),
));
?>
