<?php
// Register Sidebars
function custom_sidebars_footer()
{
    $args = array(
        'id'            => 'f1',
        'name'          => __('Footer one', 'text_domain'),
    );
    register_sidebar($args);

    $args = array(
        'id'            => 'f2',
        'name'          => __('Footer two', 'text_domain'),
    );
    register_sidebar($args);
}
add_action('widgets_init', 'custom_sidebars_footer');
