
<?php if (get_field('theme_menu_type', 'option') == 'fullscreen'): ?>

	<button class="hamburger hamburger--collapse" type="button">
	<span class="hamburger-box">
		<span class="hamburger-inner"></span>
	</span>
	</button>
<?php endif; ?>

<?php if (get_field('theme_menu_type', 'option') == 'default'): ?>
	<nav id ="navy" class="navbar navbar-expand-md navbar-light bg-light navbar-fixed-top" role="navigation">
	<div class="container">

	<!-- Brand and toggle get grouped for better mobile display -->
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
	 <span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand" href="/"><?php
    $image = get_field('theme_logo', 'option');
    if (!empty($image)): ?>
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="main_logo" />
	<?php endif; ?></a>
	 <?php
             wp_nav_menu(array(
                     'theme_location'    => 'primary',
                     'depth'             => 2,
                     'container'         => 'div',
                     'container_class'   => 'collapse navbar-collapse navbar-right flex-row-reverse',
                     'container_id'      => 'main-menu',
                     'menu_class'        => 'float-right nav navbar-nav main-menu ',
                     'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                     'walker'            => new WP_Bootstrap_Navwalker(),
             ));
             ?>
	</div>
	</nav>
<?php endif; ?>
