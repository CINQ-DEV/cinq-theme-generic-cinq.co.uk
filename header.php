<!DOCTYPE html>
<?php $image = get_field('header_image');?>
<html <?php language_attributes(); ?> class="no-js">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php if (get_field('apple_touch_57', 'option')): ?>
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php the_field('apple_touch_57', 'option'); ?>">
    <?php endif; ?>
    <?php if (get_field('apple_touch_72', 'option')): ?>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php the_field('apple_touch_72', 'option'); ?>">
    <?php endif; ?>
    <?php if (get_field('apple_touch_114', 'option')): ?>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php the_field('apple_touch_114', 'option'); ?>">
    <?php endif; ?>
    <?php if (get_field('apple_touch_144', 'option')): ?>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php the_field('apple_touch_144', 'option'); ?>">
    <?php endif; ?>
    <?php if (get_field('theme-color', 'option')): ?>
    <meta name="theme-color" content="<?php the_field('theme-color', 'option'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php the_field('apple_touch_144', 'option'); ?>">
    <?php endif; ?>
    <?php if (get_field('theme-color', 'option')): ?>
    <meta name="msapplication-TileColor" content="<?php the_field('theme-color', 'option'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php the_field('apple_touch_144', 'option'); ?>">
    <?php endif; ?>
    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>
</head>

<body class="cinq">
    <?php if (is_page_template('template-full-screen.php')) {
    ?>
    <?php

} else {
    ?>
    <header style="background-image: url('	<?php echo $image['url']; ?>')">

        <?php get_template_part('navigation'); ?>
    </header>
    <?php

}?>
    <a id="button"><i class="fas fa-chevron-up"></i></a>
    <div class="content">
