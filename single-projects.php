<?php get_header();
    /**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
 ?>
	<div id="primary" class="content-area">
<div class="container">
	<div class="row">
		<!-- start breadcrumbs -->
		<?php
        if (function_exists('yoast_breadcrumb')) {
            yoast_breadcrumb('<p id="breadcrumb">', '</p>');
        }
        ?>
<!-- end breadcrumbs -->

	</div>
  <!-- Content here -->
	<?php
    // Start the loop.
    while (have_posts()) : the_post();
    ?>

	<h1>	<?php the_title(); ?></h1>


</div>

<div class="container">
  <div class="row">
		<div class="col">
		<?php
        the_content();
        ?>
	</div>
  </div>
</div>
</div>
<?php
// End of the loop.
endwhile;
?>

<?php get_footer(); ?>
