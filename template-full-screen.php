<?php /* Template Name: Fullscreen JS */ ?>
<?php get_header(); ?>
<?php get_template_part('navigation'); ?>
  <div id="fullpage">
<?php
// check if the flexible content field has rows of data
if (have_rows('full_content')):
     // loop through the rows of data
    while (have_rows('full_content')) : the_row();
        if (get_row_layout() == 'standard'):?>
        <?php $image = get_sub_field('section_background', 'option');?>
        <div class="section" style="background:<?php the_sub_field('section_color');?>;background-image: url('	<?php echo $image['url']; ?>')">
          <div class="container">
          <div class="row">
            <div class="col">
      <h2>  <?php the_sub_field('section_title');?></h2>
        <?php the_sub_field('section_content');?>
      </div>
      </div>
</div>
        </div>
  <?php elseif (get_row_layout() == 'download'):
            $file = get_sub_field('file');
        endif;
    endwhile;
else :
    // no layouts found
endif;
?>
<div class="section fp-auto-height"><?php get_footer(); ?></div>

</div>

<script >
new fullpage('#fullpage', {
	//options here
  licenseKey: 'E8D68ACB-BC2B4441-806E2DAC-2CFAFC77',
  navigation: true,
  css3: true,
  verticalCentered: true,
});
</script>
