<!-- Default Single page template -->
<?php get_header(); ?>
<div id="primary" class="content-area container">
<div class="row">
      <div class="col">
    <!-- start breadcrumbs -->
    <?php
    if ( function_exists('yoast_breadcrumb') ) {
      yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
    }
    ?>
<!-- end breadcrumbs -->
</div>
</div>
<div class="row">
    <div class="col">
<!-- Content here -->
<?php
// Start the loop.
  while (have_posts()) : the_post();
?>

<h1>	<?php the_title(); ?></h1>

<?php
if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
//the_post_thumbnail( 'full' );
}
?>
  <?php
      the_content();
      ?>
</div>
</div>
</div>

<?php
// End of the loop.
endwhile;
?>

<?php get_footer(); ?>
